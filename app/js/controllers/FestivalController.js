
angular.module('vendini').controller('FestivalController', ['$scope', '$routeParams', 'FestivalService', function($scope, $routeParams, $festivalService) {

  var id = $routeParams.id;
  $scope.item = $festivalService.get(id);

}]);
