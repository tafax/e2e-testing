
angular.module('vendini').controller('HomeController', ['$scope', 'FestivalService', function($scope, $festivalService) {

  $scope.collection = $festivalService.all();

  $scope.add = function(event) {
    event.preventDefault();
    event.stopPropagation();
    if(event.keyCode == 13 && $scope.name){
      $festivalService.add({
        id: new Date().getTime(),
        name: $scope.name,
        description: undefined,
        comments: [],
        status: 'empty'
      });
      $scope.name = '';
      $festivalService.save();
      $scope.collection = $festivalService.all();
    }
  };

  $scope.select = function(entry) {
    $scope.selected = entry;
  };

  $scope.addDescription = function(event) {
    event.preventDefault();
    event.stopPropagation();
    if(event.keyCode == 13 && $scope.description){
      $festivalService.editDescription($scope.selected.id,  $scope.description);
      $scope.description = '';
      $scope.selected.status = 'filled';
      $festivalService.save();
    }
  };

  $scope.edit = function(event) {
    event.preventDefault();
    event.stopPropagation();
    $scope.selected.status = 'editing';
  };

  $scope.remove = function(event, id) {
    event.preventDefault();
    event.stopPropagation();
    $festivalService.remove(id);
    $scope.selected = null;
    $festivalService.save();
    $scope.collection = $festivalService.all();
  };

}]);
