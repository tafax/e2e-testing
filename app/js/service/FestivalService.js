
angular.module('vendini').service('FestivalService', function() {

  var _collection = localStorage.getItem('list') ? JSON.parse(localStorage.getItem('list')) :
    [
      {
        id: new Date('10/30/1984').getTime(),
        name: 'Coachella Festival 2016',
        description: "Coachella Festival is an annual music and arts festival held at the Empire Polo Club in Indio, California, located in the Inland Empire's Coachella Valley in the Colorado Desert.",
        comments: [
          {
            name: 'Matteo',
            message: 'So cool!'
          }
        ],
        status: 'filled'
      },
      {
        id: new Date('10/31/1984').getTime(),
        name: 'Spring Attitude 2016',
        description: undefined,
        comments: [],
        status: 'empty'
      }
    ];

  return {

    all: function() {
      return _collection;
    },

    get: function(id) {
      for(var i=0; i<_collection.length; i++) {
        if (_collection[i].id == id) {
          return _collection[i];
        }
      }
    },

    add: function(festival) {
      _collection.push(festival);
    },

    editDescription: function(id, description) {
      var festival = this.get(id);
      festival.description = description;
    },

    remove: function(id) {
      var newCollection = [];
      for(var i=0; i<_collection.length; i++) {
        if (_collection[i].id != id) {
          newCollection.push(_collection[i]);
        }
      }
      _collection = newCollection;
    },

    save: function() {
      localStorage.setItem('list', angular.toJson(_collection));
    }
  };

});
