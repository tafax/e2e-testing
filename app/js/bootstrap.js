
angular.module("vendini", ['ngRoute'])
  .config(['$routeProvider', function($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'js/templates/home.html',
        controller: 'HomeController'
      })
      .when('/festival/:id', {
        templateUrl: 'js/templates/festival.html',
        controller: 'FestivalController'
      })
      .otherwise({
        redirectTo: '/'
      });
  }]);
