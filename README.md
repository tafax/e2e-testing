E2E Testing Sample App
====================
This application provides a simple AngularJS todo list and a set of E2E tests made with [Codecption](http://codeception.com/).

## Setup
Make sure to installe the depency managers, if not:
* NodeJS: `https://nodejs.org/en/download/`
* Bower: after installing *NodeJS* `npm install -g bower`
* HttpServer: after installing *NodeJS* `npm install -g http-server`
* Composer: `https://getcomposer.org/download/`, follow the instructions. Install it in the project root.

## Selenium and ChromeDriver
* Selenium: download Selenium Server *jar* from `http://goo.gl/IHP6Qw`
* ChromeDriver: download the ChromeDriver from `https://sites.google.com/a/chromium.org/chromedriver/downloads`

Put them in you home directory for simplicity.

## Install
On the project root execute `php composer.phar install` to install the PHP dependencies.
Afterwards go to `app` directory with `cd app` and install the JS dependencies with `bower install`.

## Starts Selenium Server
Starts Selenium server with the ChromeDriver: `java -jar selenium-server-standalone-2.53.0.jar -Dwebdriver.chrome.driver=chromedriver`. You should open a shell window dedicated to the server.

## Starts the local Web Server
On project root execute `http-server app/`. You should open a dedicated shell window. This will make app available at `http://localhost:8080`.

## Run tests
On project root executes `./vendor/bin/codecpt run --env chrome --env firefox`.
