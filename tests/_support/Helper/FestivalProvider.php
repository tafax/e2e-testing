<?php

namespace Helper;

use Codeception\Module;

/**
 * Class FestivalProvider
 *
 * It declares a convenience method to get a Festival fixture.
 *
 * @package Helper
 */
class FestivalProvider extends Module
{
    /**
     * Gets a new dummy Festival.
     *
     * @return \stdClass
     */
    public function getDummyFestival()
    {
        $festival = new \stdClass();
        $festival->name = 'Festival Test';
        $festival->description = 'A description for a festival';
        
        return $festival;
    }
}