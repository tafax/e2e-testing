<?php

namespace Page;

use AcceptanceTester;

class FestivalPage
{
    // Page composition.
    public static $URL = "#/festival";
    public static $BREADCRUMB_BACK = ".breadcrumb[ng-href*='/']";

    /**
     * The tester object.
     *
     * @var AcceptanceTester
     */
    protected $tester;

    /**
     * FestivalPage constructor.
     *
     * @param AcceptanceTester $tester
     */
    public function __construct(AcceptanceTester $tester)
    {
        $this->tester = $tester;
    }

    /**
     * Goes to the page.
     */
    public function gotTo()
    {
        $I = $this->tester;
        $I->amOnPage(static::$URL);
    }

    /**
     * Clicks on home link in the breadcrumbs.
     */
    public function goHome ()
    {
        $I = $this->tester;
        $I->click([ 'css' => static::$BREADCRUMB_BACK ]);
        $I->wait(1);
    }

}
