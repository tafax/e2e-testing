<?php

namespace Page;

use AcceptanceTester;
use WebDriverKeys;

class HomePage
{
    // Page composition.
    public static $URL = "#/";
    public static $INPUT_ADD = "#add";
    public static $COLLECTION = ".collection";
    public static $COLLECTION_ITEMS = ".collection a";
    public static $CARD_TITLE = ".unipg-card-title";
    public static $ADD_DESCRIPTION = ".unipg-card a[ng-click*=edit]";
    public static $TEXT_AREA_DESCRIPTION = ".unipg-card textarea";
    public static $P_DESCRIPTION = ".unipg-card .unipg-card-section > p";
    public static $REMOVE_ACTION = ".unipg-card-subsection a[ng-click*=remove]";
    public static $FESTIVAL_LINK = ".unipg-card a[ng-href*=festival]";

    /**
     * The tester for this page.
     *
     * @var AcceptanceTester
     */
    protected $tester;

    /**
     * HomePage constructor.
     * 
     * @param AcceptanceTester $tester
     */
    public function __construct(AcceptanceTester $tester)
    {
        $this->tester = $tester;
    }

    /**
     * Goes to the page.
     */
    public function gotTo()
    {
        $I = $this->tester;
        $I->amOnPage(static::$URL);
    }

    /**
     * Adds a new Festival.
     *
     * @param $name
     */
    public function addFestival($name)
    {
        $I = $this->tester;
        $I->fillField(static::$INPUT_ADD, $name);
        $I->wait(1);
        $I->pressKey(static::$INPUT_ADD, WebDriverKeys::ENTER);
    }

    /**
     * Gets the name of the element of the collection in a specific position.
     *
     * @param $position
     *
     * @return string
     */
    public function getNameFestivalAtPosition($position)
    {
        $I = $this->tester;
        return $I->grabTextFrom(static::$COLLECTION . " a:nth-child({$position})");
    }

    /**
     * Opens a card by searching for a position into the collection.
     *
     * @param $position
     */
    public function openCardForElementAt($position)
    {
        $name = $this->getNameFestivalAtPosition($position);
        $this->openCardForElementWithText($name);
    }

    /**
     * Opens a card by searching for text into the collection.
     *
     * @param $text
     */
    public function openCardForElementWithText($text)
    {
        $I = $this->tester;
        $I->click($text, static::$COLLECTION);
        $I->waitForElement(self::$CARD_TITLE);
    }

    /**
     * Adds a description.
     *
     * @param $description
     */
    public function addDescription($description)
    {
        $I = $this->tester;
        $I->click(['css' => static::$ADD_DESCRIPTION]);
        $I->fillField(['css' => static::$TEXT_AREA_DESCRIPTION], $description);
        $I->wait(1);
        $I->pressKey(static::$TEXT_AREA_DESCRIPTION, WebDriverKeys::ENTER);
    }

    /**
     * Removes a Festival.
     */
    public function removeFestival()
    {
        $I = $this->tester;
        $I->click([ 'css' => static::$REMOVE_ACTION]);
    }

    /**
     * Clicks on link to display the Festival page.
     */
    public function clickOnFestivalPage()
    {
        $I = $this->tester;
        $I->click(['css' => static::$FESTIVAL_LINK]);
        $I->wait(1);
    }
}
