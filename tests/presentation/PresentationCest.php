<?php

class PresentationCest
{
    public function testGoToDetails(AcceptanceTester $I)
    {
        $I->amOnPage('#/');

        $I->pauseExecution();

        $I->click('Coachella Festival 2016');

        $I->pauseExecution();

        $I->canSee('Coachella Festival 2016', '.unipg-card-title');

        // Click on link to go to festival page.
        //$I->click([ 'xpath' => "//*[contains(@class,'unipg-card')]/a[contains(@ng-href, 'festival')]" ]);
        $I->click([ 'css' => '.unipg-card a[ng-href*=festival]' ]);

        $I->pauseExecution();

        $I->canSeeCurrentUrlMatches('*/festival*');
    }
}