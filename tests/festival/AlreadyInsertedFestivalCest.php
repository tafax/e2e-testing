<?php

use Page\HomePage;
use Page\FestivalPage;

/**
 * Class VendiniFestivalCest
 */
class AlreadyInsertedFestivalCest
{
    /**
     * Tests the Festival removal.
     *
     * @param AcceptanceTester $I
     * @param HomePage $homePage
     */
    public function testRemoveAFestival(AcceptanceTester $I, HomePage $page)
    {
        $page->gotTo();

        $I->pauseExecution();

        $I->seeNumberOfElementsInDOM(HomePage::$COLLECTION_ITEMS, 2);

        $name = $page->getNameFestivalAtPosition(1);
        $page->openCardForElementAt(1);
        $page->removeFestival();

        $I->seeNumberOfElementsInDOM(HomePage::$COLLECTION_ITEMS, 1);

        $I->pauseExecution();

        $I->dontSee($name);
    }

    /**
     * Tests the description addition.
     *
     * @param HomeTester $I
     * @param HomePage $homePage
     */
    public function testAddDescriptionToFestival(AcceptanceTester $I, HomePage $page)
    {
        $page->gotTo();
        
        $page->openCardForElementAt(1);
        
        $I->pauseExecution();
        
        $page->addDescription('A generic description');
        
        $I->pauseExecution();
        
        $I->see('A generic description', HomePage::$P_DESCRIPTION);
    }

    /**
     * Tests go to Festival page.
     *
     * @param AcceptanceTester $I
     * @param HomePage $homePage
     * @param FestivalPage $festivalPage
     */
    public function testGoToDetails(AcceptanceTester $I, HomePage $homePage, FestivalPage $festivalPage)
    {
        $homePage->gotTo();

        $I->pauseExecution();

        $homePage->openCardForElementAt(1);

        $I->pauseExecution();

        $homePage->clickOnFestivalPage();

        $I->pauseExecution();

        $I->seeCurrentUrlMatches("*" . FestivalPage::$URL . "*");

        $I->pauseExecution();

        $festivalPage->goHome();

        $I->pauseExecution();

        $I->seeCurrentUrlMatches("*" . HomePage::$URL . "*");
    }
}
