<?php

use Page\HomePage;
use Page\FestivalPage;
use Helper\FestivalProvider;

class NewFestivalCest
{
    /**
     * Provider class to retrieve Festival objects.
     *
     * @var FestivalProvider
     */
    private $festivalProvider;

    /**
     * Injects the test dependencies.
     *
     * @param FestivalProvider $festivalProvider
     */
    public function _inject(FestivalProvider $festivalProvider)
    {
        $this->festivalProvider = $festivalProvider;
    }

    /**
     * Tests Festival addition.
     *
     * @param AcceptanceTester $I
     * @param HomePage $homePage
     */
    public function testAddNewFestivalAndRemoveIt(AcceptanceTester $I, HomePage $homePage)
    {
        $homePage->gotTo();

        $festival = $this->festivalProvider->getDummyFestival();
        $I->dontSee($festival->name, HomePage::$COLLECTION);

        $I->pauseExecution();

        $homePage->addFestival($festival->name);

        $I->pauseExecution();

        $I->see($festival->name, HomePage::$COLLECTION);

        $homePage->openCardForElementWithText($festival->name);
        $homePage->removeFestival();

        $I->pauseExecution();

        $I->dontSee($festival->name, HomePage::$COLLECTION);
    }

    /**
     * Tests the Festival removal.
     *
     * @param AcceptanceTester $I
     * @param HomePage $homePage
     */
    public function testAddNewFestivalAndAddADescriptionAndRemoveIt(AcceptanceTester $I, HomePage $homePage)
    {
        $homePage->gotTo();

        $festival = $this->festivalProvider->getDummyFestival();
        $homePage->addFestival($festival->name);

        $I->pauseExecution();

        $homePage->openCardForElementWithText($festival->name);

        $I->pauseExecution();

        $homePage->addDescription($festival->description);

        $I->pauseExecution();

        $I->see($festival->description, HomePage::$P_DESCRIPTION);

        $homePage->removeFestival();

        $I->pauseExecution();

        $I->dontSee($festival->name, HomePage::$COLLECTION);
    }

    /**
     * Tests go to Festival page.
     *
     * @param AcceptanceTester $I
     * @param HomePage $homePage
     */
    public function testAddNewFestivalAndGoToDetails(AcceptanceTester $I, HomePage $homePage, FestivalPage $festivalPage)
    {
        $homePage->gotTo();

        $festival = $this->festivalProvider->getDummyFestival();
        $homePage->addFestival($festival->name);

        $I->pauseExecution();

        $homePage->openCardForElementWithText($festival->name);

        $I->pauseExecution();

        $homePage->clickOnFestivalPage();

        $I->pauseExecution();

        $I->seeCurrentUrlMatches("*" . FestivalPage::$URL . "*");

        $festivalPage->goHome();

        $homePage->openCardForElementWithText($festival->name);

        $I->pauseExecution();

        $homePage->removeFestival();
    }
}
